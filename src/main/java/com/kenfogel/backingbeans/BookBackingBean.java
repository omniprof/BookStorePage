package com.kenfogel.backingbeans;

import com.kenfogel.entities.Inventory;
import java.io.Serializable;
import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import javax.enterprise.context.RequestScoped;
import javax.inject.Named;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This is a backing bean. It is used when a page or form must interact with
 * beans that are not managed such as entity beans. In this example the entity
 * bean for Inventory will be manually loaded with data for the example page.
 *
 * @author Ken
 */
@Named
@RequestScoped
public class BookBackingBean implements Serializable {

    private final static Logger LOG = LoggerFactory.getLogger(BookBackingBean.class);

    private Inventory inventory;

    /**
     * Client created if it does not exist.
     *
     * @return
     */
    public Inventory getInventory() {
        if (inventory == null) {
            inventory = new Inventory();
            // Fill the entity with sample data rather than load from the database
            fillInventory();
        }
        return inventory;
    }

    private void fillInventory() {

        try {
            SimpleDateFormat dateformat = new SimpleDateFormat("dd/MM/yyyy");

            inventory.setIsbn("9780091956141");
            inventory.setDateofentry(dateformat.parse("12/01/2016"));
            inventory.setTitle("The Martian");
            inventory.setAuthors("Andy Weir");
            inventory.setPublisher("Crown Publishing");
            inventory.setDateofpublication(dateformat.parse("01/01/2011"));
            inventory.setPages(369);
            inventory.setGenre("Science Fiction");
            inventory.setImage("9780091956141.jpg");
            inventory.setCost(new BigDecimal("12.00"));
            inventory.setList(new BigDecimal("29.95"));
            inventory.setRemovalstatus(false);
        } catch (ParseException ex) {
            LOG.error("Parsing Exception \n" + ex);
        }
    }
}
